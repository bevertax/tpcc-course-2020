#include "api.hpp"

#include "scheduler.hpp"

namespace tinyfiber {

//////////////////////////////////////////////////////////////////////

void RunScheduler(FiberRoutine init) {
  Scheduler scheduler;
  scheduler.Run(std::move(init));
}

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine) {
  GetCurrentScheduler()->Spawn(std::move(routine));
}

void Yield() {
  GetCurrentScheduler()->Yield();
}

FiberId GetFiberId() {
  return GetCurrentFiber()->Id();
}

}  // namespace tinyfiber
